node {
    def git = 'git clone --single-branch -b 10072018 https://raul_gutierrez_alten@bitbucket.org/raul_gutierrez_alten/ryanair-2.git'
    try {
        currentBuild.result = 'SUCCESS'

        stage('Clone') {
            timeout(time: 2, unit: 'MINUTES') {
                if (isUnix()) {
                    deleteDir()
                    sh git
                } else {
                    deleteDir()
                    bat git
                }
            }
        }
    } catch (err) {
        echo "Caught: ${err}"
        currentBuild.result = 'FAILURE'
    }

    try {
        currentBuild.result = 'SUCCESS'

        stage('Build') {
            timeout(time: 2, unit: 'MINUTES') {
                if (isUnix()) {
                    dir("${WORKSPACE}"+'/ryanair-2') {
                        sh 'chmod 777 ./gradlew'
                        sh './gradlew clean'
                    }
                } else {
                    dir('ryanair-2') {
                        bat 'gradle clean'
                    }
                }
            }
        }
    } catch (err) {
        echo "Caught: ${err}"
        currentBuild.result = 'FAILURE'
    }

    try {
        currentBuild.result = 'SUCCESS'

        stage('Test') {
            timeout(time: 30, unit: 'MINUTES') {
                if (isUnix()) {
                    dir("${WORKSPACE}"+'/ryanair-2') {
                        sh './gradlew test aggregate'
                    }
                } else {
                    dir('\\ryanair-2') {
                        bat 'gradle test aggregate'
                    }
                }
            }
        }
    } catch (err) {
        echo "Caught: ${err}"
        currentBuild.result = 'FAILURE'
    }

    try {
        currentBuild.result = 'SUCCESS'

        stage('Serenity Report') {
            timeout(time: 2, unit: 'MINUTES') {
                if (isUnix()) {
                    echo "${WORKSPACE}"+'/ryanair-2'
                    dir("${WORKSPACE}"+'/ryanair-2') {
                        publishHTML([
                                allowMissing         : false,
                                alwaysLinkToLastBuild: false,
                                keepAll              : false,
                                reportDir            : "${WORKSPACE}"+'/ryanair-2/target/site/serenity',
                                reportFiles          : 'index.html',
                                reportName           : 'Serenity',
                                reportTitles         : ''
                        ])
                    }
                } else {
                    dir("${WORKSPACE}"+'\\ryanair-2') {
                        publishHTML([
                                allowMissing         : false,
                                alwaysLinkToLastBuild: false,
                                keepAll              : false,
                                reportDir            : '\\target\\site\\serenity',
                                reportFiles          : 'index.html',
                                reportName           : 'Serenity',
                                reportTitles         : ''
                        ])
                    }
                }

            }
        }
    } catch (err) {
        echo "Caught: ${err}"
        currentBuild.result = 'FAILURE'
    }

    try {
        currentBuild.result = 'SUCCESS'

        stage('Cucumber Report') {
            timeout(time: 2, unit: 'MINUTES') {
                if (isUnix()) {
                    dir("${WORKSPACE}"+'/ryanair-2') {
                        cucumber fileIncludePattern: 'cucumber-report.json', jsonReportDirectory: "${WORKSPACE}"+'/ryanair-2/target', sortingMethod: 'ALPHABETICAL'
                    }
                } else {
                    dir("${WORKSPACE}"+'\\ryanair-2') {
                        cucumber fileIncludePattern: 'cucumber-report.json', jsonReportDirectory: '\\target', sortingMethod: 'ALPHABETICAL'
                    }
                }
            }
        }
    } catch (err) {
        echo "Caught: ${err}"
        currentBuild.result = 'FAILURE'
    }

    try {
        currentBuild.result = 'SUCCESS'

        stage('Send email') {
            emailext body: 'Test done', subject: 'Jenkins test', to: 'raul.gutierrez@alten.es'
        }
    } catch (err) {
        echo "Caught: ${err}"
        currentBuild.result = 'FAILURE'

    }
}