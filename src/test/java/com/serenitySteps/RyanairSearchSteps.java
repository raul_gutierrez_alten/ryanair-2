package com.serenitySteps;

import com.page.*;
import net.thucydides.core.annotations.Step;

public class RyanairSearchSteps {

    private VuelosPage vuelosPage;
    private BookingPlanePage bpp;


    @Step
    public void openRyanairPage() {
        vuelosPage.open();
    }

    @Step
    public void closeCookiesMessage() {
        vuelosPage.closeCookiesMessage();
    }

    @Step
    public void selectOneWay(String typeOfFlight) {
        String typeCssPath = ("round".equalsIgnoreCase(typeOfFlight))? "div.return > input" : "div.one-way > input";
        vuelosPage.selectRadioButton(typeCssPath);
    }

    @Step
    public void searchAirportTakeOff(String country, String city) {
        vuelosPage.selectAirportTakeOff(country, city);
    }

    public void searchAirportLand(String country, String city) {
        vuelosPage.selectAirportLand(country, city);
    }

    public void selectDate(String date) {
        vuelosPage.selectDate(date);
    }

    public void selectAdultPassengers(int nAdults) {
        vuelosPage.selectAdultPassengers(nAdults);
    }

    public void selectTeenPassengers(int nTeens) {
        vuelosPage.selectTeenPassengers(nTeens);
    }

    public void selectChildPassengers(int nChildren) {
        vuelosPage.selectChildPassengers(nChildren);
    }

    public void selectBabyPassengers(int nBabies) {
        vuelosPage.selectBabyPassengers(nBabies);
    }

    public void closePassengersDiv() {
        vuelosPage.closePassengersDiv();
    }

    public void vamos() {
        vuelosPage.vamos();
    }

    public void testDepartureCity(String city)  {
        bpp.testDepartureCity(city);
    }

    public void testArrivalCity(String city)  {
        bpp.testArrivalCity(city);
    }

    public void testFlightDate(String typeOfFlight, String typeOfDate, String date)  {
        bpp.testFlightDate(typeOfFlight, typeOfDate, date);
    }

    public void acceptConditionTerms() {
        vuelosPage.acceptConditionTerms();
    }

    public void closeAcceptConditionsNotice(){
        vuelosPage.closeAcceptConditionsNotice();
    }

    public void selectFlight() {
        bpp.selectFlight();
    }

    public void selectFlightRate(String rate) {
        bpp.selectFlightRate(rate);
    }

    public void continueBuyFlight(){
        bpp.goOn();
    }

    public void reduceTheNumberOfBabiesTo(int nBabies)  {
        vuelosPage.reduceTheNumberOfBabiesTo(nBabies);
    }

    public void checkTheConditionsOfTheWebsiteMessage(String msg){
        vuelosPage.checkTheConditionsOfTheWebsiteMessage(msg);
    }
}