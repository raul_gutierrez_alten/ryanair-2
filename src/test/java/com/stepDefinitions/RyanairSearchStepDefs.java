package com.stepDefinitions;

import com.serenitySteps.RyanairSearchSteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class RyanairSearchStepDefs {

    @Steps
    RyanairSearchSteps ryanairSearchSteps;

    @Given("^I want to search a flight in Ryanair$")
    public void iWantToSearchAFlightInRyanair() {
        ryanairSearchSteps.openRyanairPage();
    }

    @When("^I close cookies message$")
    public void iCloseCookiesMessage() {
        ryanairSearchSteps.closeCookiesMessage();
    }

    @And("^I select (one-way|round)$")
    public void iSelectOneWay(String typeOfFlight) {
        ryanairSearchSteps.selectOneWay(typeOfFlight);
    }


    @And("^I select from \"([^\"]*)\" \"([^\"]*)\" to take off$")
    public void iSelectFromToTakeOff(String country, String city)  {
        ryanairSearchSteps.searchAirportTakeOff(country,city);

    }

    @And("^I select \"([^\"]*)\" \"([^\"]*)\" to land$")
    public void iSelectToLand(String country, String city)  {
        ryanairSearchSteps.searchAirportLand(country,city);
    }


    @And("^I select a date \"([^\"]*)\"$")
    public void iSelectADate(String date)  {
        ryanairSearchSteps.selectDate(date);
    }

    @And("^I select departure date \"([^\"]*)\"$")
    public void iSelectDepartureDate(String date)  {
        ryanairSearchSteps.selectDate(date);
    }

    @And("^I select return date \"([^\"]*)\"$")
    public void iSelectReturnDate(String date)  {
        ryanairSearchSteps.selectDate(date);
    }

    @And("^I select the \"([^\"]*)\" of adult passengers$")
    public void iSelectTheOfAdultPassengers(int nAdults)  {
        ryanairSearchSteps.selectAdultPassengers(nAdults);
    }

    @And("^I select the \"([^\"]*)\" of  teenage passengers$")
    public void iSelectTheOfTeenagePassengers(int nTeens)  {
        ryanairSearchSteps.selectTeenPassengers(nTeens);
    }

    @And("^I select the \"([^\"]*)\" of child passengers$")
    public void iSelectTheOfChildPassengers(int nChildren)  {
        ryanairSearchSteps.selectChildPassengers(nChildren);
    }

    @And("^I select the \"([^\"]*)\" of baby passengers$")
    public void iSelectTheOfBabyPassengers(int nBabies)  {
        ryanairSearchSteps.selectBabyPassengers(nBabies);
    }

    @And("^I close passengers select$")
    public void iClosePassengersSelect()  {
        ryanairSearchSteps.closePassengersDiv();
    }

    @And("^I accept advice flight conditions$")
    public void iAcceptAdviceFlightConditions()  {
        ryanairSearchSteps.acceptConditionTerms();
    }

    @And("^Submit to the next page$")
    public void submitToTheNextPage()  {
        ryanairSearchSteps.vamos();
    }

    @And("^I test departureCity \"([^\"]*)\"$")
    public void iTestDepartureCity(String city)  {
        ryanairSearchSteps.testDepartureCity(city);
    }

    @And("^I test arrivalCity \"([^\"]*)\"$")
    public void iTestArrivalCity(String city)  {
        ryanairSearchSteps.testArrivalCity(city);
    }

    @And("^I test (one-way|round) (departure|return) flight date \"([^\"]*)\"$")
    public void iTestFlightDate(String typeOfFlight, String typeOfDate, String date)  {
        ryanairSearchSteps.testFlightDate(typeOfFlight, typeOfDate, date);
    }

    @And("^selectFlight$")
    public void selectflight()  {
        ryanairSearchSteps.selectFlight();
    }

    @And("^select flight rate \"([^\"]*)\"$")
    public void selectFlightRate(String rate)  {
        ryanairSearchSteps.selectFlightRate(rate);
    }

    @Then("^continue buy flight$")
    public void continueBuyFlight()  {
        ryanairSearchSteps.continueBuyFlight();

    }

    @And("^I cancel advice flight conditions$")
    public void iCancelAdviceFlightConditions()  {
        ryanairSearchSteps.acceptConditionTerms();
    }

    @And("^I close the accept conditions notice$")
    public void iCloseTheAcceptConditionsNotice()  {
        ryanairSearchSteps.closeAcceptConditionsNotice();
    }

    @And("^I reduce the number of babies to zero \"([^\"]*)\"$")
    public void iReduceTheNumberOfBabiesToZero(int nBabies)  {
        ryanairSearchSteps.reduceTheNumberOfBabiesTo(nBabies);
    }

    @And("^I select the number the babies I want again \"([^\"]*)\"$")
    public void iSelectTheNumberTheBabiesIWantAgain(int nBabies)  {
        ryanairSearchSteps.selectBabyPassengers(nBabies);
    }

    @And("^I check the conditions of the website message \"([^\"]*)\"$")
    public void iCheckTheConditionsOfTheWebsiteMessage(String msg)  {
        ryanairSearchSteps.checkTheConditionsOfTheWebsiteMessage(msg);
    }
}
