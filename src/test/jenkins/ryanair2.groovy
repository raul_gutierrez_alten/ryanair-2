node {
    try {
        currentBuild.result = 'SUCCESS'

        stage('Clone') {
            timeout(time: 2, unit: 'MINUTES') {
                if (isUnix()) {
                    deleteDir()
                    sh 'jenkins'
                } else {
                    deleteDir()
                    bat 'git clone https://raul_gutierrez_alten@bitbucket.org/raul_gutierrez_alten/ryanair-2.git'
                }
            }
        }
    } catch (err) {
        echo "Caught: ${err}"
        currentBuild.result = 'FAILURE'
    }

    try {
        currentBuild.result = 'SUCCESS'

        stage('Build') {
            timeout(time: 2, unit: 'MINUTES') {
                if (isUnix()) {
                    dir('ryanair-2') {
                        sh './gradlew clean'
                    }
                } else {
                    dir('ryanair-2') {
                        bat 'gradle clean'
                    }
                }
            }
        }
    } catch (err) {
        echo "Caught: ${err}"
        currentBuild.result = 'FAILURE'
    }

    try {
        currentBuild.result = 'SUCCESS'

        stage('Test') {
            timeout(time: 30, unit: 'MINUTES') {
                if (isUnix()) {
                    dir('/ryanair-2') {
                        sh './gradlew test aggregate'
                    }
                } else {
                    dir('\\ryanair-2') {
                        bat 'gradle test aggregate'
                    }
                }
            }
        }
    } catch (err) {
        echo "Caught: ${err}"
        currentBuild.result = 'FAILURE'
    }

    try {
        currentBuild.result = 'SUCCESS'

        stage('Serenity Report') {
            timeout(time: 2, unit: 'MINUTES') {
                if (isUnix()) {
                    dir('/ryanair-2') {
                        publishHTML([
                                allowMissing         : false,
                                alwaysLinkToLastBuild: false,
                                keepAll              : false,
                                reportDir            : '/target/site/serenity',
                                reportFiles          : 'index.html',
                                reportName           : 'Serenity',
                                reportTitles         : ''
                        ])
                    }
                } else {
                    dir('C:\\Users\\raul.gutierrez\\.jenkins\\workspace\\ryanair-fe\\ryanair-2') {
                        publishHTML([
                                allowMissing         : false,
                                alwaysLinkToLastBuild: false,
                                keepAll              : false,
                                reportDir            : '\\target\\site\\serenity',
                                reportFiles          : 'index.html',
                                reportName           : 'Serenity',
                                reportTitles         : ''
                        ])
                    }
                }

            }
        }
    } catch (err) {
        echo "Caught: ${err}"
        currentBuild.result = 'FAILURE'
    }

    try {
        currentBuild.result = 'SUCCESS'

        stage('Cucumber Report') {
            timeout(time: 2, unit: 'MINUTES') {
                if (isUnix()) {
                    dir('/ryanair-2') {
                        cucumber fileIncludePattern: 'cucumber-report.json', jsonReportDirectory: '/target', sortingMethod: 'ALPHABETICAL'
                    }
                } else {
                    dir('C:\\Users\\raul.gutierrez\\.jenkins\\workspace\\ryanair-fe\\ryanair-2') {
                        cucumber fileIncludePattern: 'cucumber-report.json', jsonReportDirectory: '\\target', sortingMethod: 'ALPHABETICAL'
                    }
                }
            }
        }
    } catch (err) {
        echo "Caught: ${err}"
        currentBuild.result = 'FAILURE'
    }

    try {
        currentBuild.result = 'SUCCESS'

        stage('Send email') {
            emailext body: 'Test done', subject: 'Jenkins test', to: 'raul.gutierrez@alten.es'
        }
    } catch (err) {
        echo "Caught: ${err}"
        currentBuild.result = 'FAILURE'

    }
}