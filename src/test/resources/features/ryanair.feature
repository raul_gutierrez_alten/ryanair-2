@Ready
Feature: From Ryanair Page we will find a flight with the characteristics that we want.


#  Scenario: Search a flight in Ryanair
#    Given I want to search a flight in Ryanair
#    When I close cookies message
#    And I select one-way
#    And I select from "España" "Valladolid" to take off
#    And I select "España" "Barcelona-El Prat" to land
#    And I select a date "2018-07-29"
#    And I select the "3" of adult passengers
#    And I select the "2" of  teenage passengers
#    And I select the "3" of child passengers
#    And I select the "2" of baby passengers
#    And I close passengers select
#    And I accept advice flight conditions
#    And Submit to the next page
#    And I test departureCity "Valladolid"
#    And I test arrivalCity "Barcelona-El Prat"
#    And I test one-way departure flight date "2018-09-29"
#    And selectFlight
#    And select flight rate "Flexi Plus"

  #@test
  Scenario Outline: Search multiple
    Given I want to search a flight in Ryanair
    When I close cookies message
    And I select one-way
    And I select from "<departureCountry>" "<departureCity>" to take off
    And I select "<arrivalCountry>" "<arrivalCity>" to land
    And I select a date "<yyyy-mm-dd>"
    And I select the "<numAdultPassengers>" of adult passengers
    And I select the "<numTeenPassengers>" of  teenage passengers
    And I select the "<numChildPassengers>" of child passengers
    And I select the "<numBabyPassengers>" of baby passengers
    And I close passengers select
    And I accept advice flight conditions
    And Submit to the next page
    And I test departureCity "<departureCity>"
    And I test arrivalCity "<arrivalCity>"
    And I test one-way departure flight date "<yyyy-mm-dd>"
#    this walk out from our test case
    And selectFlight
    And select flight rate "<rate>"
    Then continue buy flight
    Examples:
      | departureCountry | departureCity | arrivalCountry | arrivalCity       | yyyy-mm-dd | numAdultPassengers | numTeenPassengers | numChildPassengers | numBabyPassengers | rate       |
     # | España           | Valladolid    | España         | Barcelona-El Prat | 2018-09-12 | 5                  | 2                 | 2                  | 2                 | Flexi Plus |
      | España           | Madrid        | Italia         | Roma Ciampino     | 2018-11-29 | 5                  | 2                 | 2                  | 2                 | Flexi Plus |
     # | España           | Madrid        | Italia         | Roma Ciampino     | 2018-10-07 | 5                  | 2                 | 2                  | 2                 | Flexi Plus |
      | España           | Madrid        | Alemania       | Berlín-Schönefeld | 2018-08-29 | 5                  | 2                 | 2                  | 2                 | Flexi Plus |

  #@test
  Scenario Outline: Search a flight, where we select and unselect the conditions
    Given I want to search a flight in Ryanair
    When I close cookies message
    And I select one-way
    And I select from "<departureCountry>" "<departureCity>" to take off
    And I select "<arrivalCountry>" "<arrivalCity>" to land
    And I select a date "<yyyy-mm-dd>"
    And I accept advice flight conditions
    And I cancel advice flight conditions
    And I check the conditions of the website message "<message>"
    And I close the accept conditions notice
    And Submit to the next page
    And I test departureCity "<departureCity>"
    And I test arrivalCity "<arrivalCity>"
    And I test one-way departure flight date "<yyyy-mm-dd>"

    Examples:
      | departureCountry | departureCity | arrivalCountry | arrivalCity | yyyy-mm-dd | message                                     |
      | Israel           | Tel Aviv      | Chipre         | Pafos       | 2018-08-18 | Acepta las Condiciones de uso del sitio web |


  #@test
  Scenario Outline: I increase the number of babies, then I reduce them and finally I increase it again. I verify the baby increase button control appear agaiin
    Given I want to search a flight in Ryanair
    When I close cookies message
    And I select one-way
    And I select from "<departureCountry>" "<departureCity>" to take off
    And I select "<arrivalCountry>" "<arrivalCity>" to land
    And I select a date "<yyyy-mm-dd>"
    And I select the "<numAdultPassengers>" of adult passengers
    And I select the "<numBabyPassengers>" of baby passengers
    And I reduce the number of babies to zero "<numBabyPassengers>"
    And I select the number the babies I want again "<numBabyPassengers>"
    And Submit to the next page
    And I test departureCity "<departureCity>"
    And I test arrivalCity "<arrivalCity>"
    And I test one-way departure flight date "<yyyy-mm-dd>"

    Examples:
      | departureCountry | departureCity | arrivalCountry | arrivalCity        | yyyy-mm-dd | numAdultPassengers | numBabyPassengers |
      | Montenegro       | Podgorica     | Bélgica        | Bruselas Charleroi | 2018-08-16 | 5                  | 3                 |



#  #@test
#  Scenario Outline: Check that I cannot flight without adults
#    Given I want to search a flight in Ryanair
#    When I close cookies message
#    And I select one-way
#    And I select from "<departureCountry>" "<departureCity>" to take off
#    And I select "<arrivalCountry>" "<arrivalCity>" to land
#    And I select a date "<yyyy-mm-dd>"
#    And Submit to the next page
#    And I test departureCity "<departureCity>"
#    And I test arrivalCity "<arrivalCity>"
#    And I test one-way departure flight date "<yyyy-mm-dd>"
#    Examples:
#      | departureCountry | departureCity | arrivalCountry | arrivalCity      | yyyy-mm-dd |
#      | Francia          | Brive         | Reino Unido    | Londres Stansted | 2018-08-18 |


  #@test
#  Scenario Outline: Search a flight where there is not connection to the arrival airport does not exist from departure airport
#    Given I want to search a flight in Ryanair
#    When I close cookies message
#    And I select one-way
#    And I select from "<departureCountry>" "<departureCity>" to take off
#    And I select "<arrivalCountry>" "<arrivalCity>" to land
#    And I select a date "<yyyy-mm-dd>"
#    And Submit to the next page
#    And I test departureCity "<departureCity>"
#    And I test arrivalCity "<arrivalCity>"
#    And I test one-way departure flight date "<yyyy-mm-dd>"
#
#    Examples:
#      | departureCountry | departureCity | arrivalCountry | arrivalCity | yyyy-mm-dd |
#      | Venezuela        | Caracas       | Alemania       | Colonia     | 2018-07-22 |
  

  #@test
  Scenario Outline: Search a return flight
    Given I want to search a flight in Ryanair
    When I close cookies message
    And I select round
    And I select from "<departureCountry>" "<departureCity>" to take off
    And I select "<arrivalCountry>" "<arrivalCity>" to land
    And I select departure date "<departure_yyyy-mm-dd>"
    And I select return date "<return_yyyy-mm-dd>"
    And I select the "<numAdultPassengers>" of adult passengers
    And Submit to the next page
    And I test departureCity "<departureCity>"
    And I test arrivalCity "<arrivalCity>"
    And I test round departure flight date "<departure_yyyy-mm-dd>"
    And I test round return flight date "<return_yyyy-mm-dd>"

    Examples:
      | departureCountry | departureCity | arrivalCountry | arrivalCity | departure_yyyy-mm-dd | return_yyyy-mm-dd | numAdultPassengers |
      | Marruecos         | Ouarzazate        | España        | Madrid        | 2018-12-04           | 2019-01-12        | 2                  |

  @test
  Scenario Outline: Test a round flight with dates in different month and not visible months
    Given I want to search a flight in Ryanair
    When I close cookies message
    And I select round
    And I select from "<departureCountry>" "<departureCity>" to take off
    And I select "<arrivalCountry>" "<arrivalCity>" to land
    And I select departure date "<departure_yyyy-mm-dd>"
    And I select return date "<return_yyyy-mm-dd>"
    And I select the "<numAdultPassengers>" of adult passengers
    And Submit to the next page
    And I test departureCity "<departureCity>"
    And I test arrivalCity "<arrivalCity>"
    And I test round departure flight date "<departure_yyyy-mm-dd>"
    And I test round return flight date "<return_yyyy-mm-dd>"

    Examples:
      | departureCountry | departureCity | arrivalCountry | arrivalCity | departure_yyyy-mm-dd | return_yyyy-mm-dd | numAdultPassengers |
      | Serbia           | Nis           | Eslovaquia     | Bratislava  | 2018-10-26           | 2019-02-08        | 2                  |