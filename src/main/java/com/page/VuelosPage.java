package com.page;

import com.libraries.Fecha;
import com.libraries.FileOperations;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.WhenPageOpens;
import net.thucydides.core.pages.PageObject;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.GregorianCalendar;
import java.util.List;

@DefaultUrl("https://www.ryanair.com/es/es/")
//@WhenPageOpens (getDriver().manage().window().maximize())
public class VuelosPage extends PageObject {

    /*************************************************
     * WEBELEMENTS SELECTOR *
     *************************************************/
    //BOTÓN PARA CERRAR EL MENSAJE DE COOKIES
    @FindBy(css = "cookie-pop-up > div > div.cookie-popup__close-btn > core-icon > div > svg > use")
    private WebElementFacade btn_cerrar_mensajes_cookies;

    //RADIO BUTTON IDA
    @FindBy(css = "div.one-way > input")
    private WebElementFacade rb_id_ida;

    //INPUT ORIGEN
    @FindBy(css = "div.route-selector-departure>div.field-type>div.field-value>div.disabled-wrap>input")
    private WebElementFacade departureInput;
    @FindBy(css = "div.left>div.three-cols")
    private WebElementFacade divCountries_selector;
    @FindBy(css = "div.right>div.core-list-small")
    private WebElementFacade divCities_selector;

    //CALENDARIO
    //Ul calendario
    @FindBy(css = "div.arrow_box>div.content>popup-content div:nth-of-type(1) ul[ng-if]")
    private WebElementFacade calendar;

    @FindAll({
            @FindBy(css = "li.calendar-view:nth-of-type(1)>ul.days"),
            @FindBy(css = "li.calendar-view:nth-of-type(2)>ul.days")
    })
    private List<WebElementFacade> months;

    @FindAll({
            @FindBy(css = "li.calendar-view:nth-of-type(1)"),
            @FindBy(css = "li.calendar-view:nth-of-type(2)")
    })
    private List<WebElementFacade> calendarView;

    @FindBy(css = "button.right")
    private WebElementFacade nextMonths;

    //PASAJEROS
    //Div Pasajeros
    @FindBy(css = "div.field-type>div>div.value")
    private WebElementFacade div_passengers;
    //Btn + Adultos
    @FindBy(css = "div.content>popup-content>div:nth-of-type(6) div.details-controls button.inc")
    private WebElementFacade btn_adults_increase;
    //Btn - Adultos
    @FindBy(css = "div.header div.details-controls button.dec:nth-of-type(1)")
    private WebElementFacade btn_adults_reduce;
    //Num adultos
    @FindBy(css = "div.content>popup-content>div:nth-of-type(6) div.details-num>input")
    private WebElementFacade numAdults;
    //Btn + Adolescentes
    @FindBy(css = "div.content>popup-content>div:nth-of-type(7) div.details-controls button:nth-of-type(2)")
    private WebElementFacade btn_teens;
    //Btn + Niños
    @FindBy(css = "div.content>popup-content>div:nth-of-type(8) div.details-controls button:nth-of-type(2)")
    private WebElementFacade btn_children;
    //Btn + Baby
    @FindBy(css = "div.content>popup-content>div:nth-of-type(9) div.details-controls button.inc")
    private WebElementFacade btn_baby_mas;
    //Btn - Baby
    @FindBy(css = "div.content>popup-content>div:nth-of-type(9)>div.details>div.details-controls button.dec")
    private WebElementFacade btn_baby_menos;
    //Div message bebés
    @FindBy(css = "div.dialog-body")
    private WebElementFacade divMessageBaby;
    //Botón aceptar aviso bebés
    @FindBy(css = "div.text-center>button.core-btn-primary")
    private WebElementFacade btn_baby_accept;

    //ACEPTAR CONDICIONES
    @FindBy(css = "div.terms-conditions span.terms-conditions-checkbox-span")
    private WebElementFacade span_condition_terms;
    @FindBy(css = "div.terms-error-tooltip__close")
    private WebElementFacade acceptConditionsNoticeCloseButton;
    @FindBy(css = "div.terms-error-tooltip>span")
    private WebElementFacade messageConditions;

    //BOTÓN VAMOS
    @FindBy(css = "button.core-btn-block:nth-of-type(2)")
    private WebElementFacade btn_vamos;

    /*************************************
     * CONSTRUCTOR *
     *************************************/

    public VuelosPage(WebDriver driver) {
        super(driver);
    }

    /**************************************
     * MÉTODOS *
     *************************************/

    @WhenPageOpens
    public void waitUntilRyanairLogoAppears() {
        WebElementFacade web = find(By.cssSelector("div.col-flight-search-right > button.core-btn-primary"));
        web.waitUntilVisible();
        WebElementFacade web2 = find(By.cssSelector("div.route-selector-departure label"));
        web2.waitUntilVisible();
    }

    @WhenPageOpens
    public void maximizeScreen() {
        getDriver().manage().window().maximize();
    }

    //Cierra el mensaje de las cookies
    public void closeCookiesMessage() {
        WebElementFacade web = FileOperations.checkVisibility(btn_cerrar_mensajes_cookies);
        web.click();
    }

    //Este método seleccionará el radio-button "IDA" que queremos
    public void selectRadioButton(String rbPath) {
        //Seleccionamos el radiobutton a través del ID
        WebElementFacade we = find(By.cssSelector(rbPath));
        we.click();
    }

    //Selecciona el aeropuerto de salida
    public void selectAirportTakeOff(String country, String city) {
        //Pulsamos en el input de Origen
        departureInput.waitUntilVisible();
        departureInput.waitUntilEnabled();
        departureInput.waitUntilPresent();
        departureInput.waitUntilClickable();
        departureInput.click();

        //Buscamos el país introducido por parámetro
        WebElementFacade divPaises = FileOperations.checkVisibility(divCountries_selector);
        FileOperations.searchCountryCity(country, divPaises, "div"); //Le pasamos el div contenedor de países, busca el país que pasamos por parámetro y le da click

        //Buscamos la ciudad introducida por parámetro
        WebElementFacade divCiudades = FileOperations.checkVisibility(divCities_selector); //Le pasamos el div contenedor de ciudades, busca la ciudad que pasamos por parámetro y le da click
        FileOperations.searchCountryCity(city, divCiudades, "span");

    }

    //Selecciona el aeropuerto de llegada
    public void selectAirportLand(String country, String city) {
        //Buscamos el país introducido por parámetro
        WebElementFacade divPaises = FileOperations.checkVisibility(divCountries_selector);
        FileOperations.searchCountryCity(country, divPaises, "div");

        //Buscamos la ciudad introducida por parámetro
        WebElementFacade divCiudades = FileOperations.checkVisibility(divCities_selector);
        FileOperations.searchCountryCity(city, divCiudades, "span");
    }

    //Selecciona la fecha que le pasamos por parámetro
    public void selectDate(String sDate) {
        //Creamos un objeto fecha con los datos de la fecha de hoy
        Fecha date = new Fecha(new GregorianCalendar());
        String mesRyanair;
        String mesFechaVuelo = date.sSelectMonth(sDate);
        List<WebElementFacade> diasSpan;
        List<WebElementFacade> diasLi;
        WebElementFacade wef;
        WebElementFacade mesBuscado = null;
        boolean mesIsVisible = false;
        int c = 1;
        List<WebElementFacade> mesesH1;
        List<WebElementFacade> mesesLi;

        //Si la fecha seleccionada es posterior a la fecha de hoy devuelve true y por tanto está bien
        if (date.compareDates(sDate)) {
            //Creamos un bucle, que se repetirá mientras la fecha que buscamos no sea visible en la web
            do {
                //Introducimos los títulos de los mesesH1 en una lista
                mesesH1 = calendar.thenFindAll(By.cssSelector("h1"));
                mesesLi = calendar.thenFindAll(By.cssSelector("li.calendar-view")); //Será un array paralelo con el ul contenedor del mes en texto y los diasSpan de dicho mas

                //Al ser una página dinámica, comprobamos que los mesesH1 que queremos sean visibles
                mesesH1.get(c).waitUntilVisible();

                //Recorremos los mesesH1 visibles
                for (WebElementFacade we : mesesH1) {
                    //Obtenemos el índice del arraylist
                    int mesesH1Index = mesesH1.indexOf(we);
                    //Comprobamos que el mes es visible
                    if (we.isDisplayed() && we.isEnabled()) {
                        mesRyanair = date.obtainMonthRyanair(we.getText()); //Obtenemos el texto de cada mes de ryanair
                        if (mesRyanair.equals(mesFechaVuelo)) { //Comprobamos que el mes que buscamos sea igual al mes que se muestra en pantalla
                            mesBuscado = mesesLi.get(mesesH1Index); //Asignamos a un objeto externo el objeto que hemos buscado
                            mesIsVisible = true; //Cambiamos visible a true
                            break;
                        }
                    }
                }

                //Si no se encuentra el mes, pulsamos siguiente mes
                if (!mesIsVisible) {
                    nextMonths.click();
                } else {
                    diasSpan = mesBuscado.thenFindAll(By.cssSelector("li.calendar-view>ul.days>li>span"));
                    diasLi = mesBuscado.thenFindAll(By.cssSelector("li.calendar-view>ul.days>li"));
                    for (WebElementFacade we : diasSpan) {
                        int diasSpanIndex = diasSpan.indexOf(we);
                        String classFecha = diasLi.get(diasSpanIndex).getAttribute("class");
                        we.waitUntilClickable();
                        we.waitUntilEnabled();
                        if (!classFecha.equals("blank unavailable") && !classFecha.equals("unavailable") && Integer.parseInt(we.getText()) == date.obtainDayDateParam(sDate)) {
                            we.click();
                            break;
                        }
                    }
                    break;
                }
                c++;
            } while (!mesIsVisible);
        }
    }

    //Seleccionamos el número de Pasajeros adultos
    public void selectAdultPassengers(int nAdults) {
        WebElementFacade divPassengers = FileOperations.checkVisibility(div_passengers);
        if (nAdults > 0) {
            divPassengers.click();
            if (nAdults != 1) {
                for (int i = 1; i < nAdults; i++) {
                    btn_adults_increase.click();
                }
            }
        }
    }

    //Seleccionamos el número de Pasajeros adolescentes
    public void selectTeenPassengers(int nTeens) {
        if (nTeens > 0) {
            for (int i = 0; i < nTeens; i++) {
                btn_teens.click();
            }
        }
    }

    //Seleccionamos el número de Pasajeros niños
    public void selectChildPassengers(int nChildren) {
        if (nChildren > 0) {
            for (int i = 0; i < nChildren; i++) {
                btn_children.click();
            }
        }
    }

    //Seleccionamos el número de Pasajeros babés
    public void selectBabyPassengers(int nBabies) {
        if (nBabies > 0) {
            btn_baby_mas.waitUntilVisible();
            btn_baby_mas.waitUntilClickable();
            for (int i = 0; i < nBabies; i++) {
                btn_baby_mas.click();
                if (i == 0) {
                    btn_baby_accept.click();
                    findBy("span[translate='foh.home.flight_search_infant_popup.message']").waitUntilNotVisible();
                }
            }
        }
    }

    public void closePassengersDiv() {
        WebElementFacade divPassengers = FileOperations.checkVisibility(div_passengers);
        divPassengers.click();
    }

    public void vamos() {
        WebElementFacade vamosBtn = FileOperations.checkVisibility(btn_vamos);
        vamosBtn.click();
    }

    //Aceptamos los términos de uso
    public void acceptConditionTerms() {
        span_condition_terms.waitUntilVisible();
        span_condition_terms.waitUntilClickable();
        span_condition_terms.click();
    }

    public void closeAcceptConditionsNotice() {
        acceptConditionsNoticeCloseButton.waitUntilVisible();
        acceptConditionsNoticeCloseButton.waitUntilClickable();
        acceptConditionsNoticeCloseButton.click();
    }

    public void checkTheConditionsOfTheWebsiteMessage(String msg) {
        messageConditions.waitUntilVisible();
        Assert.assertEquals(msg, messageConditions.getText());
    }

    public void reduceTheNumberOfBabiesTo(int nBabies) {
        if (nBabies > 0) {
            btn_baby_menos.waitUntilVisible();
            btn_baby_menos.waitUntilClickable();
            for (int i = nBabies; i > 0; i--) {
                btn_baby_menos.click();
            }
        }
    }
}
