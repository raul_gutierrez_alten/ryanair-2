package com.page;

import com.libraries.Fecha;
import com.libraries.FileOperations;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Thread.sleep;

public class BookingPlanePage extends PageObject {
    //CONSTANTS
    @FindBy(css = "div.flight-title h1 span:nth-of-type(1)")
    private WebElementFacade departureCity;

    @FindBy(css = "div.flight-title h1 span:nth-of-type(2)")
    private WebElementFacade arrivalCity;

    @FindBy(css = "form div.active div.date")
    private WebElementFacade divFlights;

    @FindBy(css = "flight-list#outbound > form div.active div.date")
    private WebElementFacade departureFlightDate;

    @FindBy(css = "flight-list#inbound > form div.active div.date")
    private WebElementFacade returnFlightDate;


    @FindBy(css = "div.slide:nth-of-type(6)>div.carousel-item>div.date")
    private WebElementFacade dateFlight;
    @FindBy(css = "div.flights-table")
    private WebElementFacade flightInfoDiv;

    @FindBy(css = "button.flight-selector__listing-footer-button-next")
    private WebElementFacade btnContinuar;

    @FindBy(css = "div.flight-header__min-price div.flights-table-price")
    private WebElementFacade buttonSelectFlight;

    @FindAll({
            @FindBy(css = "div.flights-table-fares__wrapper--loaded>div:nth-of-type(1)"),
            @FindBy(css = "div.flights-table-fares__wrapper--loaded>div:nth-of-type(2)"),
            @FindBy(css = "div.flights-table-fares__wrapper--loaded>div:nth-of-type(3)")
    })
    private List<WebElementFacade> flightRates;

    @FindBy(css = " button[ng-disabled='isSearchButtonDisabled()']")
    private WebElementFacade flightButton;

    public void testDepartureCity(String city){
        try {
            sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ArrayList<String> lists = new ArrayList<>(getDriver().getWindowHandles());
        System.out.println(lists.size());
        if(lists.size()>=2){
            getDriver().switchTo().window(lists.get(lists.size()-1));
        }
        if(departureCity.isDisplayed()){
            departureCity.waitUntilVisible();
            departureCity.waitUntilEnabled();
            Assert.assertEquals("The departure city selected is not equals than the city showed in the Booking Plane Page", departureCity.getText(), city);
        }

    }

    public void testArrivalCity(String city){
        arrivalCity.waitUntilVisible();
        Assert.assertEquals("The arrival city selected is not equals than the city showed in the Booking Plane Page", arrivalCity.getText(), city);
    }

    public void testFlightDate(String typeOfFlight, String typeOfDate, String sDate){
        divFlights.waitUntilVisible();
        divFlights.waitUntilClickable();
        if (typeOfFlight.equals("one-way")) {
            FileOperations.testFlightResult(divFlights, sDate);
        } else {
            if (typeOfDate.equals("departure")) {
                FileOperations.testFlightResult(departureFlightDate, sDate);
            } else {
                FileOperations.testFlightResult(returnFlightDate, sDate);
            }
        }
    }

    public void selectFlight() {
        flightInfoDiv.waitUntilVisible();
        flightInfoDiv.waitUntilEnabled();
        flightInfoDiv.waitUntilClickable();
        List<WebElement> flights = flightInfoDiv.findElements(By.cssSelector("div.ranimate-flights-table div.flight-header__min-price span.flights-table-price__price"));
        float pxBarato = Integer.MAX_VALUE;
        float px = 0;
        for (WebElement we : flights) {
            String sPxVuelo = FileOperations.changeComaByAPoint(we.getText());
            sPxVuelo = sPxVuelo.substring(0, sPxVuelo.length() - 2);
            px = Float.parseFloat(sPxVuelo);
            if (px < pxBarato) {
                pxBarato = px;
            }
        }
        List<WebElement> btnPx = flightInfoDiv.findElements(By.cssSelector("div.ranimate-flights-table div.flight-header__min-price div.core-btn-primary"));

        for (WebElement we : btnPx) {
            WebElement spanPx = we.findElement(By.cssSelector("span.flights-table-price__price"));
            String sSpanPx = FileOperations.changeComaByAPoint(spanPx.getText());
            sSpanPx = sSpanPx.substring(0, sSpanPx.length() - 2);
            float iSpanPx = Float.parseFloat(sSpanPx);
            if (iSpanPx == pxBarato) {
                we.click();
                break;
            }
        }
    }

    public void selectFlightRate(String rate) {
        flightRates.get(0).waitUntilVisible();
        flightRates.get(0).waitUntilClickable();
        WebElement spanFlightRate;
        for (WebElementFacade wef : flightRates) {
            spanFlightRate = wef.findElement(By.cssSelector("div.flights-table-fares__head>span"));
            if (spanFlightRate.getText().equals(rate)) {
                wef.waitUntilClickable();
                wef.click();
            }
        }
    }

    public void goOn() {
        btnContinuar.waitUntilVisible();
        btnContinuar.waitUntilEnabled();
        btnContinuar.waitUntilClickable();
        btnContinuar.click();
    }


}


