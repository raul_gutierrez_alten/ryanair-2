package com.libraries;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Fecha {
    GregorianCalendar gc;

    public Fecha(GregorianCalendar gc){
        this.gc = gc;
    }

    public boolean compareDates(String otherDate){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String sTodayDate = getTodayYear()+"-0"+getTodayMonth()+"-"+getTodayDay();

        try {
            Date today = new java.util.Date(sdf.parse(sTodayDate).getTime());
            Date otherDay = new java.util.Date(sdf.parse(otherDate).getTime());

            if(otherDay.after(today) || otherDay.equals(today)){
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;

    }

    public int getTodayDay(){
        return gc.get(Calendar.DAY_OF_MONTH);
    }

    public int getTodayMonth(){
        return gc.get(Calendar.MONTH)+1;
    }

    public int getTodayYear(){
        return gc.get(Calendar.YEAR);
    }

    public String sSelectMonth(String date){
        String[] splitDate = date.split("-");
        int monthDate = Integer.parseInt(splitDate[1]);
        switch(monthDate){
            case 1:
                return "Enero";
            case 2:
                return "Febrero";
            case 3:
                return "Marzo";
            case 4:
                return "Abril";
            case 5:
                return "Mayo";
            case 6:
                return "Junio";
            case 7:
                return "Julio";
            case 8:
                return "Agosto";
            case 9:
                return "Septiembre";
            case 10:
                return "Octubre";
            case 11:
                return "Noviembre";
            default:
                return "Diciembre";
        }
    }

    public int intSelectMonth(String month){
        switch(month){
            case "Ene":
                return 1;
            case "Feb":
                return 2;
            case "Mar":
                return 3;
            case "Abr":
                return 4;
            case "May":
                return 5;
            case "Jun":
                return 6;
            case "Jul":
                return 7;
            case "Ago":
                return 8;
            case "Sep":
                return 9;
            case "Oct":
                return 10;
            case "Nov":
                return 11;
            default:
                return 12;
        }
    }

    public boolean testDate(String dateRyanair, String dateParam){
        //Separamos los datos de la fecha que aparece en la página de compra de Ryanair
        String[] dataDate = dateRyanair.split(" ");
        if(dataDate.length > 1){
            //Obtenemos el día de  la página de Ryanair
            int ryanDay = Integer.parseInt(dataDate[1]);
            //Obtenemos el mes de la página de ryanair sin el punto final
            String sRyanMonth = dataDate[2];
            //Quitamos el punto final del mes y parseamos el dato a un número entero
            sRyanMonth = sRyanMonth.substring(0,sRyanMonth.length()-1);
            int ryanMonth = intSelectMonth(sRyanMonth);

            //Obtenemos el mes de la fecha que pasamos como Parámetro
            int paramDay = obtainDayDateParam(dateParam);
            int paramMonth = obtainMonthDateParam(dateParam);
            if(paramDay==ryanDay &&  ryanMonth==paramMonth){
                return true;
            }
        }
        return false;
    }

    public String obtainMonthRyanair(String sDate){
        String[] datos = sDate.split(" ");
        return datos[0];
    }

    public String obtainYearRyanair(String sDate){
        String[] datos = sDate.split(" ");
        return datos[1];
    }

    public int obtainDayDateParam(String sDate){
        String[] datos = sDate.split("-");
        return Integer.parseInt(datos[2]);
    }

    public int obtainMonthDateParam(String sDate){
        String[] datos = sDate.split("-");
        return Integer.parseInt(datos[1]);
    }

    public String obtainYearDateParam(String sDate){
        String[] datos = sDate.split("-");
        return datos[0];
    }
}
