package com.libraries;

import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.GregorianCalendar;
import java.util.List;

public class FileOperations {

    public static WebElementFacade checkVisibility(WebElementFacade web){
        web.waitUntilVisible();
        return web;
    }

    public static void searchCountryCity(String countrycity, WebElementFacade div, String elementTagName){
        //Buscamos el país introducido por parámetro
        WebElementFacade divPaises = FileOperations.checkVisibility(div);
        List<WebElement> countries = divPaises.findElements(By.tagName(elementTagName));
        boolean exists = false;
        WebElement weCityCountry = null;
        for(WebElement we: countries){
            if(we.getText().equals(countrycity)){
                exists = true;
                weCityCountry = we;
                break;
            }
        }

        if(exists){
            weCityCountry.click();
        }else{
            Assert.assertTrue("No existen vuelos entre los destinos introducidos", exists);
        }
    }

    public static String changeComaByAPoint(String s){
        return s.replaceAll(",",".");
    }

    public static void testFlightResult(WebElementFacade wef, String sDate){
        Fecha f = new Fecha(new GregorianCalendar());
        wef.waitUntilVisible();
        boolean dateOK = f.testDate(wef.getText(), sDate);
        if(dateOK){
            wef.click();
        }else{
            Assert.assertEquals("The date selected is not equals than the date showed in the Booking Plane Page", wef.getText(), sDate);
        }
    }

}

